const user_Find = (ob,cb) => 
{
    let result =false;
    for(let index = 0 ;index < ob.length ; index++)
    {
        if(ob[index] !== undefined)     //checking if element is undefined
        {
            result = cb(ob[index]);     //calling cb only for defined values of array
        }
            if(result === true)
            {
                return ob[index];
            }
            else if(index+1 === ob.length)
            {
                return undefined;       //returns undefined if no matches found in array
            }
    }
};

module.exports = user_Find;