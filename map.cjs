const user_Map = ( arr , cb ) =>
{
    let new_Arr = [];                           //New array to store data after callBack function processes.
    const len_Arr = arr.length;                 //taking length of array as Constant.
                    
    for(let index = 0 ; index < len_Arr ; index++)
    {
        
        if(arr[index] !== undefined)                //checking for undefined element so that it is not processed bu Callback function.
        {
            new_Arr.push(cb(arr[index] , index , arr));
        }
        else
        {
            new_Arr.push(undefined);
        }
        
    };
    return new_Arr;
};

module.exports = user_Map;