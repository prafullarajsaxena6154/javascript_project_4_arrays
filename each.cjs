const user_Each = ( arr , cb ) =>
{                           
    const len_Arr = arr.length;                    //taking length of array as Constant.
    for(let index = 0 ; index < len_Arr ; index++)
    {
        if(arr[index]!== undefined)                //If value of element undefined, skips the element(s)
        {
            cb(arr[index] , index);
        }
    };
};

module.exports = user_Each;