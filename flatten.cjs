let fin_Array = [];
const user_Flatten = (ob,depth=1) =>
{
    
    for(let index = 0 ; index < ob.length ; index++)
    {
        if(Array.isArray(ob[index]) && depth>0)
        {
            let new_depth = depth-1;
            user_Flatten(ob[index],new_depth);
        }
        else
        {
            if(index in ob)
            {
                fin_Array.push(ob[index]);
            }
        }
    }
    return fin_Array;
};




module.exports = user_Flatten;