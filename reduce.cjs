const reduce_Funct = (arr, cb, starting_Val) =>
{
    let start_Index=0
    
    if(starting_Val === undefined){
        starting_Val=arr[0]
        start_Index=1

    }
    for(let index= start_Index;index<arr.length;index++)
    {
        starting_Val = cb(starting_Val,arr[index],index,arr);
    }
    return starting_Val;
}

module.exports = reduce_Funct;