const user_Filter = (ob,cb) => 
{
    let fin_Array = [];                
    let result = false;
    for(let index = 0 ;index < ob.length ; index++)
    {
        if(ob[index] !== undefined)     //checking if element is undefined
        {
            result = cb(ob[index],index,ob);        //calling cb only for defined values of array
        }
            if(result === true)
            {
                fin_Array.push( ob[index] );    //pushing into the final array to be returned
            }
    }
    return fin_Array;
};

module.exports = user_Filter;